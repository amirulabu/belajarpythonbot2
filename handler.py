import json
import os
import sys

here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, "./vendored"))

import requests
import db_operation as db

TOKEN = os.environ['TELEGRAM_TOKEN']
BASE_URL = "https://api.telegram.org/bot{}".format(TOKEN)
GROUP_ID = os.environ['TELEGRAM_GROUP_ID']


def edit_message(response_dict):
    # chat_id is the id of the person not bot!
    # response_dict = {"chat_id": chat_id, "message_id": message_id, "text": text }
    url = BASE_URL + "/editMessageText"
    req = requests.post(url, json=response_dict)
    return req

def send_message(response_dict):
    # response_dict = {"chat_id": chat_id, "text": text}
    # or
    # response_dict = {"chat_id": chat_id, "text": text, "reply_markup": reply_markup}
    url = BASE_URL + "/sendMessage"
    req = requests.post(url, json=response_dict)
    return req

def group_invite_link(chat_id=GROUP_ID):
    # receives chat id and generate new invite link
    url = BASE_URL + "/exportChatInviteLink"
    response_dict = {"chat_id": chat_id}
    req = requests.post(url, json=response_dict)
    return req.json()["result"]

def add_question(response_dict,question):
    if "text" in response_dict:
        text = response_dict["text"]
        text = "{} \n{}".format(text,question)
        response_dict["text"] = text
    else:
        response_dict["text"] = question
    return response_dict

def prep_message(chat_id,text):
    response_dict = { "chat_id": chat_id,"text": text }
    return response_dict

def prep_keyboard(response_dict,reply_markup):
    reply_markup = {"inline_keyboard":reply_markup}
    response_dict.update(reply_markup=reply_markup)
    return response_dict

def logging(**kwargs):
    for k,v in kwargs.items():
        print(k,":",v)

def logging_request(**kwargs):
    for k,v in kwargs.items():
        print(k,":",v.json())

def main(event,context=None):
    try:
        logging(event=event)
        data = json.loads(event["body"])
        if "message" in data:
            message = str(data["message"]["text"])
            chat_id = data["message"]["chat"]["id"]
            user = data["message"]["chat"]
            logging(user=db.add_or_identify_user(user))
            first_name = data["message"]["chat"]["first_name"]
            response_dict = prep_message(
                            chat_id,
                            "Please /start, {}".format(first_name)
                            )

            if "start" in message:

                send_message({"chat_id": os.environ['TELEGRAM_MIRUL'], "text": "{} just started the test".format(first_name)})
                send_message({"chat_id": os.environ['TELEGRAM_KAMAL'], "text": "{} just started the test".format(first_name)})

                text = "Hello {}, please answer the following questions".format(first_name)
                question = "Siapakah yang mencipta Python?"
                buttons = [
                    [{"text":"Guido van Rossum","callback_data":"q1-c"},
                    {"text":"Google","callback_data":"q1-w"}],
                    [{"text":"Matz","callback_data":"q1-w"},
                    {"text":"Dennis Ritchie","callback_data":"q1-w"}]
                ]

                response_dict = prep_message(chat_id,text)
                response_dict = add_question(response_dict, question)
                response_dict = prep_keyboard(response_dict,buttons)

            logging_request(start=send_message(response_dict))

        elif "callback_query" in data:
            chat_id = data["callback_query"]["message"]["chat"]["id"]
            user = data["callback_query"]["message"]["chat"]
            first_name = data["callback_query"]["message"]["chat"]["first_name"]
            message_id = data["callback_query"]["message"]["message_id"]
            callback_data = data["callback_query"]["data"].split("-")

            db.add_result(user,callback_data[0], callback_data[1])

            if callback_data[0] == "q1":
                # edit previous question with ok
                response_dict = {"chat_id": chat_id, "message_id": message_id, "text": "Siapakah yang mencipta Python?" }
                edit_message(response_dict)
                # send question 2
                text = "Apakah laman web rasmi Python?"
                buttons = [
                    [{"text":"python.com","callback_data":"q2-w"},
                    {"text":"python.net","callback_data":"q2-w"}],
                    [{"text":"python.org","callback_data":"q2-c"},
                    {"text":"python-lang.com","callback_data":"q2-w"}]
                ]
                response_dict = {"chat_id": chat_id, "message_id": message_id, "text": text }
                response_dict = prep_keyboard(response_dict,buttons)
                send_message(response_dict)
            elif callback_data[0] == "q2":
                # edit previous question with ok
                response_dict = {"chat_id": chat_id, "message_id": message_id, "text": "Apakah laman web rasmi Python?" }
                edit_message(response_dict)
                # send question 3
                text = "Nyatakan versi Python yang terkini"
                buttons = [[
                    {"text":"2.7","callback_data":"q3-w"},
                    {"text":"3.5","callback_data":"q3-w"},
                    {"text":"3.6","callback_data":"q3-w"},
                    {"text":"3.7","callback_data":"q3-c"}
                ]]
                response_dict = {"chat_id": chat_id, "message_id": message_id, "text": text }
                response_dict = prep_keyboard(response_dict,buttons)
                send_message(response_dict)
            elif callback_data[0] == "q3":
                # edit previous question with ok
                response_dict = {"chat_id": chat_id, "message_id": message_id, "text": "Nyatakan versi Python yang terkini" }
                edit_message(response_dict)
                # send question 4
                text = "Yang manakah antara berikut bukan libraries dalam python"
                buttons = [
                    [{"text":"requests","callback_data":"q4-w"},
                    {"text":"django","callback_data":"q4-w"}],
                    [{"text":"laravel","callback_data":"q4-c"}]
                ]
                response_dict = {"chat_id": chat_id, "message_id": message_id, "text": text }
                response_dict = prep_keyboard(response_dict,buttons)
                send_message(response_dict)
            elif callback_data[0] == "q4":
                # edit previous question with ok
                response_dict = {"chat_id": chat_id, "message_id": message_id, "text": "Yang manakah antara berikut bukan libraries dalam python" }
                edit_message(response_dict)

                send_message({"chat_id": os.environ['TELEGRAM_MIRUL'], "text": "{} just completed the test".format(first_name)})
                send_message({"chat_id": os.environ['TELEGRAM_KAMAL'], "text": "{} just completed the test".format(first_name)})

                # send results
                correct_ans = db.check_user_questions(user)
                text = "This is your result \nYou have {} correct answers".format(correct_ans)
                if correct_ans == 4:
                    text = text + str("\nYou may enter the group with this link: {}").format(group_invite_link())
                else:
                    text = text + str("\nSorry, please try again.")
                response_dict = {"chat_id": chat_id, "message_id": message_id, "text": text }
                send_message(response_dict)

    except Exception as e:
        logging(exception=e)

    return {"statusCode": 200}