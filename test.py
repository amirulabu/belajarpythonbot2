import unittest
import os
from handler import (edit_message,send_message,group_invite_link,add_question,
                    prep_message,prep_keyboard,logging,logging_request)

import requests
# Put your telegram id here to test
TELEGRAM_ID = os.environ['TELEGRAM_ID']
message_id = 0

class HandlerTestCase(unittest.TestCase):
    """Tests for handler.py"""

    def setUp(self):
        self.request = send_message({"chat_id": TELEGRAM_ID, "message_id": 1, "text": "Test send_message"})
        self.message_id = self.request.json()['result']['message_id']

    def test_send_message(self):
        """Does it send a successful request?"""
        self.assertIsInstance(self.request ,requests.models.Response)
        self.assertEqual(self.request.status_code,200)
        self.assertEqual(self.request.json()['result']['chat']['id'], int(TELEGRAM_ID))

    def test_edit_message(self):
        """Does it edit the previous message?"""
        r = edit_message({"chat_id": TELEGRAM_ID, "message_id": self.message_id, "text": "Test edit_message"})
        self.assertIsInstance(r,requests.models.Response)
        self.assertEqual(r.status_code,200)
        self.assertEqual(r.json()['result']['chat']['id'], int(TELEGRAM_ID))
    

if __name__ == '__main__':
    unittest.main()